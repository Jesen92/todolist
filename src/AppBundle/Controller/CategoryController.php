<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CategoryController
 * @package AppBundle\Controller
 * @Route("/category")
 */
class CategoryController extends Controller
{
    /**
     * @Route("/index")
     */
    public function indexAction()
    {
      $categories = $this->getDoctrine()
        ->getRepository('AppBundle:Category')
        ->findAll();

        return $this->render('@App\Category\index.html.twig', array(
            'categories' => $categories
        ));
    }

    /**
     * @Route("/show/{id}")
    */
    public function showAction($id)
    {
      $category = $this->findCategoryById($id);

      return $this->render('@App/Category/show.html.twig', array(
        'category' => $category,
        'todos' => $category->getTodos()
      ));
    }

    /**
     * @Route("/edit/{id}")
    */    
    public function editAction($id, Request $request)
    {
      $category = $this->findCategoryById($id);

      $form = $this->createForm(CategoryType::class, $category, array('method' => 'PUT'));
      $form->add('submit', SubmitType::class, [
        'label' => 'Update',
        'attr' => ['class' => 'btn btn-success pull-right'],
      ]);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $this->persistCategory($category);

        $this->addFlash(
          'notice','Todo List item has been created!'
        );

        return $this->redirectToRoute('app_category_show', [
          'id' => $category->getId()
        ]);
      }

      return $this->render('@App/Category/new.html.twig', array(
        'form' => $form->createView()
      ));
    }

    /**
     * @Route("/update/{id}")
    */    
    public function updateAction($id, Request $request)
    {
      return $this->render('@App/Category/update.html.twig');
    }

    /**
     * @Route("/new")
    */    
    public function newAction(Request $request)
    {
      $category = new Category;
      $form = $this->createForm(CategoryType::class, $category);
      $form->add('submit', SubmitType::class, [
        'label' => 'Create',
        'attr' => ['class' => 'btn btn-success pull-right'],
      ]);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $this->persistCategory($category);

        $this->addFlash(
          'notice','Todo List item has been created!'
        );

        return $this->redirectToRoute('app_category_show', [
          'id' => $category->getId()
        ]);
      }

      return $this->render('@App/Category/new.html.twig', array(
        'form' => $form->createView()
      ));
    }

    /**
     * @Route("/create")
    */    
    public function createAction(Request $request)
    {
      return $this->render('@App/Category/index.html.twig');
    }

    /**
     * @Route("/destroy/{id}")
    */    
    public function destroyAction($id)
    {
      return $this->render('@App/Category/index.html.twig');
    }

    ////////////////////////////////////////////PRIVATE METHODS

    private function findCategoryById($id)
    {
      return $this->getDoctrine()
              ->getRepository('AppBundle:Category')
              ->find($id);
    }

    private function persistCategory($category)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($category);
      $entityManager->flush();
    }
}
