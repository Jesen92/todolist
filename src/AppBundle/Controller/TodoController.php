<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\UserRestrictions;
use AppBundle\Entity\Category;
use AppBundle\Entity\Todo;
use AppBundle\Form\TodoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class TodoController extends Controller implements UserRestrictions
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
      $todos = $this->getUser()->getTodos();
      $sharedTodos = $this->getUser()->getSharedTodos();

      return $this->render('@App/Todo/index.html.twig', array(
        'todos' => $todos,
        'sharedTodos' => $sharedTodos
      ));
    }

    /**
     * @Route("/show/{id}")
    */
    public function showAction($id)
    {
      $todo = $this->findTodoById($id);

      return $this->render('@App/Todo/show.html.twig', array(
        'todo' => $todo
      ));
    }

    /**
     * @Route("/edit/{id}")
    */
    public function editAction($id,Request $request)
    {
      $todo = $this->findTodoById($id);

      $form = $this->createForm(TodoType::class, $todo, array('method' => 'PUT'));
      $form->add('submit', SubmitType::class, [
        'label' => 'Update',
        'attr' => ['class' => 'btn btn-success pull-right'],
      ]);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $this->persistTodo($todo);

        $this->addFlash(
          'notice','Todo List item has been updated!'
        );

        return $this->redirectToRoute('app_todo_show', [
          'id' => $todo->getId()
        ]);
      }

      return $this->render('@App/Todo/edit.html.twig', array(
        'form' => $form->createView()
      ));
    }

    /**
     * @Route("/new")
    */
    public function newAction(Request $request)
    {
      $todo = new Todo;
      $form = $this->createForm(TodoType::class, $todo);
      $form->add('submit', SubmitType::class, [
        'label' => 'Create',
        'attr' => ['class' => 'btn btn-success pull-right'],
      ]);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $todo->setUser($this->getUser());
        $this->persistTodo($todo);

        $this->addFlash(
          'notice','Todo List item has been created!'
        );

        return $this->redirectToRoute('app_todo_show', [
          'id' => $todo->getId()
        ]);
      }

      return $this->render('@App/Todo/new.html.twig', array(
        'form' => $form->createView()
      ));
    }

    /**
     * @Route("/destroy/{id}")
    */
    public function destroyAction($id)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $todo = $entityManager->getRepository(Todo::class)->find($id);

      $entityManager->remove($todo);
      $entityManager->flush();

      $this->addFlash(
        'notice','Todo List item has been deleted!'
      );

      return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/settodostatus/{id}/{status}", name="set_status")
     */
    public function setTodoStatus($id, $status, Request $request)
    {
      $todo = $this->findTodoById($id);

      $todo->setStatus($status);
      $this->persistTodo($todo);

      return $this->redirect(
        $request->headers->get('referer')
      );
    }

    //////////////////////////////////////////////////PRIVATE METHODS

    private function findTodoById($id)
    {
      return $this->getDoctrine()
        ->getRepository('AppBundle:Todo')
        ->find($id);
    }

    private function persistTodo($todo)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($todo);
      $entityManager->flush();
    }
}
