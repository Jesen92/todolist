<?php
/**
 * Created by PhpStorm.
 * User: Jesenovic
 * Date: 11/04/2018
 * Time: 20:52
 */

namespace AppBundle\Form;

use AppBundle\AppBundle;
use AppBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use AppBundle\Entity\Category;
use AppBundle\Entity\Todo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TodoType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('name', TextType::class,
        array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
      ->add('description', TextAreaType::class,
        array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
      ->add('category', EntityType::class,
        array('class' => Category::class,
          'choice_label' => 'category',
          'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
      ->add('priority', ChoiceType::class,
        array('choices' => array('Low' => 'low', 'Normal' => 'normal', 'High' => 'high', 'Highest' => 'highest'),
          'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
      ->add('dueDate', DateTimeType::class,
        array( 'attr' => array('class' => '', 'style' => 'margin-bottom:15px')))
      ->add('sharedUsers', EntityType::class,
        array('class' => User::class,
          'choice_label' => 'users',
          'multiple' => true,
          'required' => false,
          'label' => "Share ya worries with ya homies...",
          'attr' => array('class' => 'chosen-select', 'style' => 'margin-bottom:15px;')))
      ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults([
      'data_class' => Todo::class,
    ]);
  }
}