<?php

namespace AppBundle\EventSubscriber;

use AppBundle\Controller\UserRestrictions;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class PermissionSubscriber implements EventSubscriberInterface
{
  /**
   * @var FilterControllerEvent $event
   */
  private $event;
  private $container;
  private $token;

  public function __construct(ContainerInterface $container)
  {
    $this->container = $container;
    $this->token = $container->get('security.token_storage')->getToken();
  }

  public function onKernelController(FilterControllerEvent $event)
  {
    $this->setEvent($event);
    $controller = $event->getController();

    if (!$this->validateBeforeActionMethod($controller)) {return;}
    if ($this->userHasPermission()) {return;}
    throw new AccessDeniedHttpException("Can't change what is not yours...dawg!");
  }

  /**
   * @return array('eventName' => array('editAction'), array('destroyAction')))
   */
  public static function getSubscribedEvents()
  {
    return array(
      KernelEvents::CONTROLLER => 'onKernelController',
    );
  }

  //////////////////////////////////////////////////////PRIVATE METHODS

  private function validateBeforeActionMethod($controller)
  {
    return $controller[0] instanceof UserRestrictions
      && array_search($controller[1], $this->container->getParameter('methods')) !== false ?
        true : false;
  }

  private function userHasPermission()
  {
    $event = $this->getEvent();
    $todo_id = $event->getRequest()->attributes->get('id');
    $user = $this->token->getUser();

    //$user_todo_ids = array_column($user->getTodos(), "id");

    //return array_search($todo_id, $user_todo_ids) ? true : false;
    return true;
  }

  /**
   * @return FilterControllerEvent
   */
  private function getEvent()
  {
    return $this->event;
  }

  /**
   * @param FilterControllerEvent $event
   */
  private function setEvent($event)
  {
    $this->event = $event;
  }
}